Rails.application.routes.draw do
  get 'students/index'

  resources :projects do
    resources :students
  end
  
  root "projects#index"
end
