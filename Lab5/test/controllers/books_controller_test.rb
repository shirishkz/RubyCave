require 'test_helper'

class BooksControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @book = books(:one)
  end

  test 'should get index' do
    get books_url
    assert_response :success
  end

  test 'should get new' do
    sign_in users(:one)
    get new_book_url
    assert_response :success
  end

  test 'should create book' do
    sign_in users(:one)
    assert_difference('Book.count') do
      post books_url, params: { book: { category_id: @book.category_id, description: @book.description,
                                        name: 'book', price: @book.price } }
    end

    assert_redirected_to book_url(Book.last)
  end

  test 'should not create book' do
    sign_in users(:one)
    assert_no_difference('Book.count') do
      post books_url, params: { book: { category_id: @book.category_id, description: @book.description,
                                        name: @book.name, price: @book.price } }
    end
    assert_response :success
    assert_template 'new'
    assert_select 'li', 'Name has already been taken'
  end

  test 'should show book' do
    get book_url(@book)
    assert_response :success
  end

  test 'should get edit' do
    sign_in users(:one)
    get edit_book_url(@book)
    assert_response :success
  end

  test 'should update book' do
    sign_in users(:one)
    patch book_url(@book), params: { book: { category_id: @book.category_id, description: @book.description,
                                             name: @book.name, price: @book.price } }
    assert_redirected_to book_url(@book)
  end

  test 'should validate book on update' do
    sign_in users(:one)
    patch book_url(@book), params: { book: { category_id: @book.category_id, description: @book.description,
                                             name: books(:two).name, price: @book.price } }
    assert_response :success
    assert_template 'edit'
    assert_select 'li', 'Name has already been taken'
  end

  test 'should destroy book' do
    sign_in users(:one)
    assert_difference('Book.count', -1) do
      delete book_url(@book)
    end

    assert_redirected_to books_url
  end
end
