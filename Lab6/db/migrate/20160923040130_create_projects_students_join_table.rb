class CreateProjectsStudentsJoinTable < ActiveRecord::Migration[5.0]
  def self.up
    create_table :projects_students, :id => false do |t|
      t.integer :project_id
      t.integer :student_id
    end
    add_index :projects_students, [:project_id, :student_id]
  end

  def self.down
    drop_table :projects_students
  end
end
