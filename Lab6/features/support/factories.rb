FactoryGirl.define do

  factory :teacher, class: User do
    email 'joe_teacher@ait.asia'
    password 'funny123'
    password_confirmation 'funny123'
  end

  factory :project do
    name 'My favorite project'
    url 'http://somewhere.com'
  end

  factory :student do
    name 'John Snow'
    studentid '123456'
    end
end